package com.pracasinfosys.jeevansanskaar.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.barteksc.pdfviewer.PDFView;

/**
 * Created by Paru on 8/aa/2018.
 */

public class AntaSudharActivity extends AppCompatActivity {

    Toolbar mtolbars;
    PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.antasudhar);

        mtolbars = (Toolbar) findViewById(R.id.ToolBars);
        setSupportActionBar(mtolbars);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        pdf();
    }

        public void pdf(){
            pdfView=(PDFView)findViewById(R.id.tritayaSatra);
            pdfView.fromAsset("Anta Sudhaar.pdf").load();

        }


    }

