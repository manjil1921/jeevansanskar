package com.pracasinfosys.jeevansanskaar.app;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tritaya,
             parichaya,
             riti_riwaj,
             janmotsav;

    ImageView img1;
    LinearLayout linear,
                 linear1,
                 linear2,
                 linear3,
                 linear4,
                 linear5,
                 linear6,
                 linear7,
                 linear8,
                 linear9,
                 linear10,
                 linear11;

    Toolbar toolbars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbars =(Toolbar)findViewById(R.id.ToolBars);

        setSupportActionBar(toolbars);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        linear=(LinearLayout)findViewById(R.id.linear);
        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, TritayaSatraActivity.class);
                startActivity(mintent);
            }
        });
//        tritaya=(TextView)findViewById(R.id.tritaya);
//        tritaya.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mintent = new Intent(MainActivity.this, TritayaSatraActivity.class);
//                startActivity(mintent);
//            }
//        });
//
//        img1=(ImageView)findViewById(R.id.img1);
//        img1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mintent = new Intent(MainActivity.this, TritayaSatraActivity.class);
//                startActivity(mintent);
//            }
//        });

        linear1=(LinearLayout) findViewById(R.id.linear1);
        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, ParichayaActivity.class);
                startActivity(mintent);
            }
        });

        linear2=(LinearLayout) findViewById(R.id.linear2);
        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, RitiRiwajActivity.class);
                startActivity(mintent);
            }
        });

        linear3=(LinearLayout) findViewById(R.id.linear3);
        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, JanmotsavSanskaarActivity.class);
                startActivity(mintent);
            }
        });

        linear4=(LinearLayout) findViewById(R.id.linear4);
        linear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, BibahaActivity.class);
                startActivity(mintent);
            }
        });

        linear5=(LinearLayout) findViewById(R.id.linear5);
        linear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, GrihaActivity.class);
                startActivity(mintent);
            }
        });

        linear6=(LinearLayout) findViewById(R.id.linear6);
        linear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, AntaSudharActivity.class);
                startActivity(mintent);
            }
        });

        linear7=(LinearLayout) findViewById(R.id.linear7);
        linear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, BadeBartaActivity.class);
                startActivity(mintent);
            }
        });

        linear8=(LinearLayout) findViewById(R.id.linear8);
        linear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, DaanDharmaActivity.class);
                startActivity(mintent);
            }
        });

        linear9=(LinearLayout) findViewById(R.id.linear9);
        linear9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, NetyaKarmaActivity.class);
                startActivity(mintent);
            }
        });

        linear10=(LinearLayout) findViewById(R.id.linear10);
        linear10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, GhareluActivity.class);
                startActivity(mintent);
            }
        });

        linear11=(LinearLayout) findViewById(R.id.linear11);
        linear11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(MainActivity.this, NavyaGeetActivity.class);
                startActivity(mintent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menudashbaord_rate:
                try {
                    Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
                break;
            case R.id.menu_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = "Check it out. This is awesome app https://play.google.com/store/apps/details?id="+getPackageName();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"About Duhabi Gaupalika app");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                break;
            case R.id.menu_exit:
                finish();
                //Intent minten = new Intent(MainActivity.this, Main2Activity.class);
                // startActivity(minten);
                break;
            case R.id.menudashbaord_update:
                try {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNeutralButton("Rate Us?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                dialog.cancel();

            }
        });
      /*  builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });*/
        AlertDialog alert = builder.create();
        alert.show();
    }

}






